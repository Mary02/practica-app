import React from "react";
import img1 from "../Imagenes/avatar-anisha.png";
import img2 from "../Imagenes/avatar-ali.png";
import img3 from "../Imagenes/avatar-richard.png";

import "./Card.css";

const Component = (props) => {
  return (

    <div className="text-said">
      <div className="Text">
        <h2>What they’ve said </h2>

        {/* Tarjetas */}
        <div className="said-card">
          <div className="card">
            <img className="Img-card" src={img1} alt="imperfil" />
            <h2>Anisha Li</h2>
            <p>
              “Manage has supercharged our team’s workflow. The ability to
              maintain visibility on larger milestones at all times keeps
              everyone motivated.”{" "}
            </p>
          </div>
          <div className="card">
            <img className="Img-card" src={img2} alt="imperfil" />
            <h2>Ali Bravo</h2>
            <p>
              “We have been able to cancel so many other subscriptions since
              using Manage. There is no more cross-channel confusion and
              everyone is much more focused.”{" "}
            </p>
          </div>
          <div className="card">
            <img className="Img-card" src={img3} alt="imperfil" />
            <h2> Richard Watts</h2>
            <p>
              “Manage allows us to provide structure and process. It keeps us
              organized and focused. I can’t stop recommending them to everyone
              I talk to!”
            </p>
          </div>
        </div>

        <div className="container">
          <button className="botonn">Get started </button>
        </div>
        <div className="section">
            <div className ="section-two"> <h2>Simplify how your team works today</h2><button className="botonn-one">Get started </button>
            

    </div>
    </div>
    </div>
    </div>
  


  );
};
export default Component;
